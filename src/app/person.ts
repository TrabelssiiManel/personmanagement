/**
 * Define a Person class for working with employees
 */

export class Person {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
}
