import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Person } from '../person';
import { PersonsService } from '../persons.service';

@Component({
  selector: 'app-update-person',
  templateUrl: './update-person.component.html',
  styleUrls: ['./update-person.component.css']
})
/**
 * UpdatePersonComponent is used to update an existing person,
 * we first get the person object using REST API and populate in HTML form via data binding. 
 * User can edit the person form data and submit the form
 */
export class UpdatePersonComponent implements OnInit {
  id: number;
  person : Person;
  constructor(private route: ActivatedRoute,private router: Router,
    private personService : PersonsService) { }
/**
 * we get the person firt from data by his ID
 */
  ngOnInit(): void {
    this.person = new Person();
    this.id = this.route.snapshot.params['id'];
    
    this.personService.getPerson(this.id)
      .subscribe(data => {
        console.log(data)
        this.person = data;
      }, error => console.log(error));
  }
  /**
   * update the existing person the redirect to list person
   */
  updatePerson() {
    this.personService.updatePerson(this.id, this.person)
      .subscribe(data => {
        console.log(data);
        this.person = new Person();
        this.gotoList();
      }, error => console.log(error));
  }

  onSubmit() {
    this.updatePerson();  
  }

  gotoList() {
    this.router.navigate(['/persons']);
  }
}
