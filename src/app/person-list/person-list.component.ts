import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faSort } from '@fortawesome/free-solid-svg-icons';
import { Person } from '../person';
import { PersonsService } from '../persons.service';

@Component({
  selector: 'app-person-list',
  templateUrl: './person-list.component.html',
  styleUrls: ['./person-list.component.css']
})
/**
 * PersonListComponent it's a component 
 * which will be used to display a list of persons, create a new person,
 *  update and delete a person.
 */
export class PersonListComponent implements OnInit {
  faSort= faSort;
  persons : Person[];
  firstName : any;
  constructor(private personservice: PersonsService,
    private router: Router ) { }

  ngOnInit(): void {
    this.reloadData();
  }
  /**
   * get All persons from service
   */
  reloadData() {
     this.personservice.getPersonsList().subscribe(res=>{
       this.persons=res;
     });
  }
  /**
   * delete person by Id
   */
  deletePerson(id: number) {
    this.personservice.deletePerson(id)
      .subscribe(
        data => {
          console.log(data);
          this.reloadData();
        },
        error => console.log(error));
  }
/**
 * redirect to personDetails by ID */
  PersonDetails(id: number){
    this.router.navigate(['details', id]);
  }

  /**
   * redirect to update person by ID
   */
  UpdatePerson(id: number){
    this.router.navigate(['update', id]);
  }
  /**
   * Search Person by FirstName
   */
  Search(){
    if(this.firstName == ""){
      this.ngOnInit();
    }
    else{
      this.persons = this.persons.filter(res=>{
        return res.firstName.toLocaleLowerCase().match(this.firstName.toLocaleLowerCase());
      })

    }
  }
  /**
   * sorting list asc and desc
   */
  key : String = 'id';
  reverse : boolean = false;
  sort(key){
    this.key = key;
    this.reverse = !this.reverse;
  }
}
